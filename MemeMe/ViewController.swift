//
//  ViewController.swift
//  MemeMe
//
//  Created by Sothearith Sreang on 4/9/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    // MARK: View objects
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var galleryButton: UIButton!
    
    @IBOutlet weak var topTextField: UITextField!
    @IBOutlet weak var bottomTextField: UITextField!
    
    @IBOutlet weak var topToolbar: UIStackView!
    @IBOutlet weak var bottomToolbar: UIStackView!
    
    
    // MARK: UI attributes
    
    let memeTextAttributes: [String: Any] = [
        NSAttributedStringKey.strokeColor.rawValue: UIColor.black,
        NSAttributedStringKey.foregroundColor.rawValue: UIColor.white,
        NSAttributedStringKey.font.rawValue: UIFont(name: "HelveticaNeue-CondensedBlack", size: 50)!,
        NSAttributedStringKey.strokeWidth.rawValue: 5
    ]
    var textFieldFirstTimeEdit: [Int: Bool] = [:]
    
    
    // MARK: Init views
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        enableButtons()
        setUpTextFields()
    }
    
    private func enableButtons() {
        cameraButton.isEnabled = UIImagePickerController.isSourceTypeAvailable(.camera)
        galleryButton.isEnabled = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        cancelButton.isEnabled = false
        shareButton.isEnabled = false
    }
    
    private func setUpTextFields() {
        setUpTextField(topTextField)
        setUpTextField(bottomTextField)
    }
    
    private func setUpTextField(_ textField: UITextField) {
        textField.defaultTextAttributes = memeTextAttributes
        textField.textAlignment = .center
        view.bringSubview(toFront: textField)
        textFieldFirstTimeEdit[textField.tag] = true
        
        textField.delegate = self
    }
    
    // MARK: Subscribe to keyboard changes
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        unSubscribeToKeyboardNotifications()
    }
    
    private func subscribeToKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    private func unSubscribeToKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        view.frame.origin.y -= getKeyboardHeight(notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        view.frame.origin.y += getKeyboardHeight(notification)
    }
    
    func getKeyboardHeight(_ notification:Notification) -> CGFloat {
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue // of CGRect
        return keyboardSize.cgRectValue.height
    }
    
    // MARK: Save image
    
    @IBAction func openShare(_ sender: Any) {
        if let meme = saveMeme() {
            let controller = UIActivityViewController(activityItems: [meme.memedImage], applicationActivities: nil)
            controller.popoverPresentationController?.sourceView = self.view
            self.present(controller, animated: true, completion: nil)
        }
    }

    private func saveMeme() -> Meme? {
        if let image = imageView.image {
            return Meme(
                topText: topTextField.text ?? "",
                bottomText: bottomTextField.text ?? "",
                originalImage: image,
                memedImage: generateMemedImage()
            )
        } else {
            return nil
        }
    }
    
    private func generateMemedImage() -> UIImage {
        // Hide toolbar
        topToolbar.isHidden = true
        bottomToolbar.isHidden = true
        
        // Render view to an image
        UIGraphicsBeginImageContext(view.frame.size)
        view.drawHierarchy(in: self.view.frame, afterScreenUpdates: true)
        let memedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndPDFContext()
        
        // Show toolbar
        topToolbar.isHidden = false
        bottomToolbar.isHidden = false
        
        return memedImage
    }
    
    
    // MARK: Click actions
    @IBAction func openCamera(_ sender: Any) {
        openImagePicker(.camera)
    }
    
    @IBAction func openGallery(_ sender: Any) {
        openImagePicker(.photoLibrary)
    }
    
    private func openImagePicker(_ sourceType: UIImagePickerControllerSourceType) {
        let controller = UIImagePickerController()
        controller.sourceType = sourceType
        controller.allowsEditing = false
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = pickedImage
            imageView.contentMode = .scaleAspectFit
            
            // Enable Share and Cancel button
            shareButton.isEnabled = true
            cancelButton.isEnabled = true
            
            picker.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func clearImage(_ sender: Any) {
        imageView.image = nil
        topTextField.text = "Top"
        bottomTextField.text = "Bottom"
        
        // Disable buttons
        shareButton.isEnabled = false
        cancelButton.isEnabled = false
        
        textFieldFirstTimeEdit.removeAll()
    }
    
    // MARK: Text Field delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textFieldFirstTimeEdit[textField.tag] ?? false {
            textField.text = ""
        }
        
        textFieldFirstTimeEdit[textField.tag] = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

