//
//  Meme.swift
//  MemeMe
//
//  Created by Sothearith Sreang on 6/9/18.
//  Copyright © 2018 Thearith. All rights reserved.
//

import Foundation
import UIKit

struct Meme {
    var topText: String
    var bottomText: String
    var originalImage: UIImage
    var memedImage: UIImage
}
